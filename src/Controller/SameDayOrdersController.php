<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SameDayOrders Controller
 *
 * @property \App\Model\Table\SameDayOrdersTable $SameDayOrders
 */
class SameDayOrdersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Stores', 'Users', 'Suppliers', 'Products']
        ];
        $sameDayOrders = $this->paginate($this->SameDayOrders);

        $this->set(compact('sameDayOrders'));
        $this->set('_serialize', ['sameDayOrders']);
    }

    /**
     * View method
     *
     * @param string|null $id Same Day Order id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $sameDayOrder = $this->SameDayOrders->get($id, [
            'contain' => ['Stores', 'Users', 'Suppliers', 'Products', 'Invoices']
        ]);

        $this->set('sameDayOrder', $sameDayOrder);
        $this->set('_serialize', ['sameDayOrder']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $sameDayOrder = $this->SameDayOrders->newEntity();
        if ($this->request->is('post')) {
            $sameDayOrder = $this->SameDayOrders->patchEntity($sameDayOrder, $this->request->data);
            if ($this->SameDayOrders->save($sameDayOrder)) {
                $this->Flash->success(__('The {0} has been saved.', 'Same Day Order'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Same Day Order'));
            }
        }
        $stores = $this->SameDayOrders->Stores->find('list', ['limit' => 200]);
        $users = $this->SameDayOrders->Users->find('list', ['limit' => 200]);
        $suppliers = $this->SameDayOrders->Suppliers->find('list', ['limit' => 200]);
        $products = $this->SameDayOrders->Products->find('list', ['limit' => 200]);
        $this->set(compact('sameDayOrder', 'stores', 'users', 'suppliers', 'products'));
        $this->set('_serialize', ['sameDayOrder']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Same Day Order id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $sameDayOrder = $this->SameDayOrders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $sameDayOrder = $this->SameDayOrders->patchEntity($sameDayOrder, $this->request->data);
            if ($this->SameDayOrders->save($sameDayOrder)) {
                $this->Flash->success(__('The {0} has been saved.', 'Same Day Order'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Same Day Order'));
            }
        }
        $stores = $this->SameDayOrders->Stores->find('list', ['limit' => 200]);
        $users = $this->SameDayOrders->Users->find('list', ['limit' => 200]);
        $suppliers = $this->SameDayOrders->Suppliers->find('list', ['limit' => 200]);
        $products = $this->SameDayOrders->Products->find('list', ['limit' => 200]);
        $this->set(compact('sameDayOrder', 'stores', 'users', 'suppliers', 'products'));
        $this->set('_serialize', ['sameDayOrder']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Same Day Order id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $sameDayOrder = $this->SameDayOrders->get($id);
        if ($this->SameDayOrders->delete($sameDayOrder)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Same Day Order'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Same Day Order'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
