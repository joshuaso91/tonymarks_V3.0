<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * StoresUsers Controller
 *
 * @property \App\Model\Table\StoresUsersTable $StoresUsers
 */
class StoresUsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Stores']
        ];
        $storesUsers = $this->paginate($this->StoresUsers);

        $this->set(compact('storesUsers'));
        $this->set('_serialize', ['storesUsers']);
    }

    /**
     * View method
     *
     * @param string|null $id Stores User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $storesUser = $this->StoresUsers->get($id, [
            'contain' => ['Users', 'Stores']
        ]);

        $this->set('storesUser', $storesUser);
        $this->set('_serialize', ['storesUser']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $storesUser = $this->StoresUsers->newEntity();
        if ($this->request->is('post')) {
            $storesUser = $this->StoresUsers->patchEntity($storesUser, $this->request->data);
            if ($this->StoresUsers->save($storesUser)) {
                $this->Flash->success(__('The {0} has been saved.', 'Stores User'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Stores User'));
            }
        }
        $users = $this->StoresUsers->Users->find('list', ['limit' => 200]);
        $stores = $this->StoresUsers->Stores->find('list', ['limit' => 200]);
        $this->set(compact('storesUser', 'users', 'stores'));
        $this->set('_serialize', ['storesUser']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Stores User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $storesUser = $this->StoresUsers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $storesUser = $this->StoresUsers->patchEntity($storesUser, $this->request->data);
            if ($this->StoresUsers->save($storesUser)) {
                $this->Flash->success(__('The {0} has been saved.', 'Stores User'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Stores User'));
            }
        }
        $users = $this->StoresUsers->Users->find('list', ['limit' => 200]);
        $stores = $this->StoresUsers->Stores->find('list', ['limit' => 200]);
        $this->set(compact('storesUser', 'users', 'stores'));
        $this->set('_serialize', ['storesUser']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Stores User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $storesUser = $this->StoresUsers->get($id);
        if ($this->StoresUsers->delete($storesUser)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Stores User'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Stores User'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
