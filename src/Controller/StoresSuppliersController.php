<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * StoresSuppliers Controller
 *
 * @property \App\Model\Table\StoresSuppliersTable $StoresSuppliers
 */
class StoresSuppliersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Stores', 'Suppliers']
        ];
        $storesSuppliers = $this->paginate($this->StoresSuppliers);

        $this->set(compact('storesSuppliers'));
        $this->set('_serialize', ['storesSuppliers']);
    }

    /**
     * View method
     *
     * @param string|null $id Stores Supplier id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $storesSupplier = $this->StoresSuppliers->get($id, [
            'contain' => ['Stores', 'Suppliers']
        ]);

        $this->set('storesSupplier', $storesSupplier);
        $this->set('_serialize', ['storesSupplier']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $storesSupplier = $this->StoresSuppliers->newEntity();
        if ($this->request->is('post')) {
            $storesSupplier = $this->StoresSuppliers->patchEntity($storesSupplier, $this->request->data);
            if ($this->StoresSuppliers->save($storesSupplier)) {
                $this->Flash->success(__('The {0} has been saved.', 'Stores Supplier'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Stores Supplier'));
            }
        }
        $stores = $this->StoresSuppliers->Stores->find('list', ['limit' => 200]);
        $suppliers = $this->StoresSuppliers->Suppliers->find('list', ['limit' => 200]);
        $this->set(compact('storesSupplier', 'stores', 'suppliers'));
        $this->set('_serialize', ['storesSupplier']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Stores Supplier id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $storesSupplier = $this->StoresSuppliers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $storesSupplier = $this->StoresSuppliers->patchEntity($storesSupplier, $this->request->data);
            if ($this->StoresSuppliers->save($storesSupplier)) {
                $this->Flash->success(__('The {0} has been saved.', 'Stores Supplier'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Stores Supplier'));
            }
        }
        $stores = $this->StoresSuppliers->Stores->find('list', ['limit' => 200]);
        $suppliers = $this->StoresSuppliers->Suppliers->find('list', ['limit' => 200]);
        $this->set(compact('storesSupplier', 'stores', 'suppliers'));
        $this->set('_serialize', ['storesSupplier']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Stores Supplier id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $storesSupplier = $this->StoresSuppliers->get($id);
        if ($this->StoresSuppliers->delete($storesSupplier)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Stores Supplier'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Stores Supplier'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
