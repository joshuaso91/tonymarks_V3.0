<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ProductsSuppliers Controller
 *
 * @property \App\Model\Table\ProductsSuppliersTable $ProductsSuppliers
 */
class ProductsSuppliersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Suppliers', 'Products']
        ];
        $productsSuppliers = $this->paginate($this->ProductsSuppliers);

        $this->set(compact('productsSuppliers'));
        $this->set('_serialize', ['productsSuppliers']);
    }

    /**
     * View method
     *
     * @param string|null $id Products Supplier id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $productsSupplier = $this->ProductsSuppliers->get($id, [
            'contain' => ['Suppliers', 'Products']
        ]);

        $this->set('productsSupplier', $productsSupplier);
        $this->set('_serialize', ['productsSupplier']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $productsSupplier = $this->ProductsSuppliers->newEntity();
        if ($this->request->is('post')) {
            $productsSupplier = $this->ProductsSuppliers->patchEntity($productsSupplier, $this->request->data);
            if ($this->ProductsSuppliers->save($productsSupplier)) {
                $this->Flash->success(__('The {0} has been saved.', 'Products Supplier'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Products Supplier'));
            }
        }
        $suppliers = $this->ProductsSuppliers->Suppliers->find('list', ['limit' => 200]);
        $products = $this->ProductsSuppliers->Products->find('list', ['limit' => 200]);
        $this->set(compact('productsSupplier', 'suppliers', 'products'));
        $this->set('_serialize', ['productsSupplier']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Products Supplier id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $productsSupplier = $this->ProductsSuppliers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $productsSupplier = $this->ProductsSuppliers->patchEntity($productsSupplier, $this->request->data);
            if ($this->ProductsSuppliers->save($productsSupplier)) {
                $this->Flash->success(__('The {0} has been saved.', 'Products Supplier'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Products Supplier'));
            }
        }
        $suppliers = $this->ProductsSuppliers->Suppliers->find('list', ['limit' => 200]);
        $products = $this->ProductsSuppliers->Products->find('list', ['limit' => 200]);
        $this->set(compact('productsSupplier', 'suppliers', 'products'));
        $this->set('_serialize', ['productsSupplier']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Products Supplier id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $productsSupplier = $this->ProductsSuppliers->get($id);
        if ($this->ProductsSuppliers->delete($productsSupplier)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Products Supplier'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Products Supplier'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
