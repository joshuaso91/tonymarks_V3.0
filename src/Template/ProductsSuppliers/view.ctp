<section class="content-header">
  <h1>
    <?php echo __('Products Supplier'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
    <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false])?>
    </li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-info"></i>
                <h3 class="box-title"><?php echo __('Information'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <dl class="dl-horizontal">
                                                                                                        <dt><?= __('Supplier') ?></dt>
                                <dd>
                                    <?= $productsSupplier->has('supplier') ? $productsSupplier->supplier->name : '' ?>
                                </dd>
                                                                                                                <dt><?= __('Product') ?></dt>
                                <dd>
                                    <?= $productsSupplier->has('product') ? $productsSupplier->product->name : '' ?>
                                </dd>
                                                                                                                        <dt><?= __('Notes') ?></dt>
                                        <dd>
                                            <?= h($productsSupplier->notes) ?>
                                        </dd>
                                                                                                                                    
                                            
                                                                                                                                                            <dt><?= __('Product Price') ?></dt>
                                <dd>
                                    <?= $this->Number->format($productsSupplier->product_price) ?>
                                </dd>
                                                                                                
                                                                                                        <dt><?= __('Date Entered') ?></dt>
                                <dd>
                                    <?= h($productsSupplier->date_entered) ?>
                                </dd>
                                                                                                                                                                                                            
                                            
                                    </dl>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>
<!-- div -->

</section>
