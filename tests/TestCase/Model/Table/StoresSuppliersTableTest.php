<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StoresSuppliersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StoresSuppliersTable Test Case
 */
class StoresSuppliersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StoresSuppliersTable
     */
    public $StoresSuppliers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.stores_suppliers',
        'app.stores',
        'app.orders',
        'app.same_day_orders',
        'app.products',
        'app.products_stores',
        'app.suppliers',
        'app.products_suppliers',
        'app.users',
        'app.stores_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('StoresSuppliers') ? [] : ['className' => 'App\Model\Table\StoresSuppliersTable'];
        $this->StoresSuppliers = TableRegistry::get('StoresSuppliers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StoresSuppliers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
