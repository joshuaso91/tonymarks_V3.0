<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ProductsStores Controller
 *
 * @property \App\Model\Table\ProductsStoresTable $ProductsStores
 */
class ProductsStoresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Stores', 'Products']
        ];
        $productsStores = $this->paginate($this->ProductsStores);

        $this->set(compact('productsStores'));
        $this->set('_serialize', ['productsStores']);
    }

    /**
     * View method
     *
     * @param string|null $id Products Store id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $productsStore = $this->ProductsStores->get($id, [
            'contain' => ['Stores', 'Products']
        ]);

        $this->set('productsStore', $productsStore);
        $this->set('_serialize', ['productsStore']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $productsStore = $this->ProductsStores->newEntity();
        if ($this->request->is('post')) {
            $productsStore = $this->ProductsStores->patchEntity($productsStore, $this->request->data);
            if ($this->ProductsStores->save($productsStore)) {
                $this->Flash->success(__('The {0} has been saved.', 'Products Store'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Products Store'));
            }
        }
        $stores = $this->ProductsStores->Stores->find('list', ['limit' => 200]);
        $products = $this->ProductsStores->Products->find('list', ['limit' => 200]);
        $this->set(compact('productsStore', 'stores', 'products'));
        $this->set('_serialize', ['productsStore']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Products Store id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $productsStore = $this->ProductsStores->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $productsStore = $this->ProductsStores->patchEntity($productsStore, $this->request->data);
            if ($this->ProductsStores->save($productsStore)) {
                $this->Flash->success(__('The {0} has been saved.', 'Products Store'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Products Store'));
            }
        }
        $stores = $this->ProductsStores->Stores->find('list', ['limit' => 200]);
        $products = $this->ProductsStores->Products->find('list', ['limit' => 200]);
        $this->set(compact('productsStore', 'stores', 'products'));
        $this->set('_serialize', ['productsStore']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Products Store id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $productsStore = $this->ProductsStores->get($id);
        if ($this->ProductsStores->delete($productsStore)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Products Store'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Products Store'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
